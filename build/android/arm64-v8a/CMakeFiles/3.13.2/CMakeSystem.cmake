set(CMAKE_HOST_SYSTEM "Darwin-18.2.0")
set(CMAKE_HOST_SYSTEM_NAME "Darwin")
set(CMAKE_HOST_SYSTEM_VERSION "18.2.0")
set(CMAKE_HOST_SYSTEM_PROCESSOR "x86_64")

include("/Users/avaleanu/Desktop/vuforia/vuforia-sdk/samples/UVCDriver/cmake/android.toolchain.arm64-v8a.cmake")

set(CMAKE_SYSTEM "Android-22")
set(CMAKE_SYSTEM_NAME "Android")
set(CMAKE_SYSTEM_VERSION "22")
set(CMAKE_SYSTEM_PROCESSOR "aarch64")

set(CMAKE_ANDROID_NDK "/Users/avaleanu/Library/Android/sdk/ndk-bundle")
set(CMAKE_ANDROID_STANDALONE_TOOLCHAIN "")
set(CMAKE_ANDROID_ARCH "arm64")
set(CMAKE_ANDROID_ARCH_ABI "arm64-v8a")
set(CMAKE_ANDROID_ARCH_HEADER_TRIPLE "aarch64-linux-android")
set(CMAKE_ANDROID_NDK_DEPRECATED_HEADERS "1")

set(CMAKE_CROSSCOMPILING "TRUE")

set(CMAKE_SYSTEM_LOADED 1)
